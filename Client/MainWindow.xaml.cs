﻿using GleamTech.VideoUltimate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Interop;

namespace Client
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Thread th;
        const int PORT = 1237;
        public MainWindow()
        {
            InitializeComponent();
            VideoFrameReader videoFrameReader = new VideoFrameReader(@"C:\Users\theng\source\repos\StreamVideo\yt1s.com - Spread Happiness Short Animation Clip.mp4.webm");
            th = new Thread(() => { VideoFrame(videoFrameReader); });
            th.Start();
        }

        private void VideoFrame(VideoFrameReader videoFrameReader)
        {
            {
                BitmapSource source = null;
                foreach (var frame in videoFrameReader)
                {
                    using (frame)
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            videoStream.Source = loadBitmap(frame);
                        }));
                    }
                }
            }
        }

        public static BitmapSource loadBitmap(System.Drawing.Bitmap source)
        {
            IntPtr ip = source.GetHbitmap();
            BitmapSource bs = null;
            bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ip,
               IntPtr.Zero, Int32Rect.Empty,
               System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            return bs;
        }

        private void sentBtn_Click(object sender, RoutedEventArgs e)
        {
            string stringIP = ip.Text;
            if (!string.IsNullOrWhiteSpace(stringIP))
            {
                try
                {
                    byte[] data = new byte[1024];

                    IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(stringIP), PORT);
                    Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                    byte[] buffer = Encoding.UTF8.GetBytes("oke");

                    server.SendTo(buffer, buffer.Length, SocketFlags.None, ipep);

                    Write("Message sent to server");

                    IPEndPoint sender_ = new IPEndPoint(IPAddress.Any, 0);
                    EndPoint remote = (EndPoint)(sender_);

                    buffer = new byte[1024];
                    int recv = server.Receive(buffer);

                    Write(Encoding.UTF8.GetString(buffer));
                }
                catch (Exception ex)
                {
                    Write(ex.Message);
                }
            }
        }

        private void Write(string msg)
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
            }));
        }

        private void message_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                sentBtn_Click(sender, e);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            th.Abort();
        }

    }
}
