﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int listenPort;
        Thread thread;
        UdpClient listener;
        IPEndPoint groupEP;
        string stringPort;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StartListener(int listenPort)
        {
            if (this.listener != null)
            {
                return;
            }
            IPEndPoint recv = new IPEndPoint(IPAddress.Any, listenPort);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            this.listener = new UdpClient(listenPort);
            this.groupEP = new IPEndPoint(IPAddress.Any, listenPort);

            try
            {
                Write("Waiting for client........", true);
                byte[] bytes = listener.Receive(ref groupEP);

                Write($"Received broadcast from {groupEP} :", false);
                Write($" {Encoding.UTF8.GetString(bytes, 0, bytes.Length)}", true);

                listener.Send(bytes, bytes.Length, groupEP);
            }
            catch (SocketException e)
            {
                Console.WriteLine(e);
            }
        }

        private void Write(string msg, bool newLine)
        {
            if (newLine)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    messages.AppendText(msg + Environment.NewLine);
                }));
            }
            else
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    messages.AppendText(msg);
                }));
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (listener != null)
            {
                listener.Close();
                this.thread.Abort();
            }
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(port.Text) && this.stringPort != port.Text)
            {
                if (this.listener != null)
                {
                    this.listener.Close();
                    this.listener = null;
                    this.thread.Abort();
                    messages.Document.Blocks.Clear();
                }
                this.stringPort = port.Text;
                try
                {
                    listenPort = int.Parse(this.stringPort);
                }
                catch (Exception ex)
                {
                    Write(ex.Message, true);
                }
                this.thread = new Thread(() => StartListener(listenPort));
                thread.Start();
            }
        }

        private void port_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                startBtn_Click(sender, e);
            }
        }
    }
}
