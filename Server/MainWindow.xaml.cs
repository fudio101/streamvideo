﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int PORT = 1237;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StartListener(int listenPort)
        {

            UdpClient udpClient = new UdpClient();
            udpClient.Client.Bind(new IPEndPoint(IPAddress.Any, listenPort));
            IPEndPoint from = new IPEndPoint(IPAddress.Any, 0);

            Write("Waiting for client........", true);

            Task.Run(() =>
            {
                while (true)
                {
                    var recvBuffer = udpClient.Receive(ref from);
                    this.Write(Encoding.UTF8.GetString(recvBuffer), true);

                    var data = Encoding.UTF8.GetBytes("ABCD");
                    udpClient.Send(data, data.Length, from);
                }
            });
        }

        private void Write(string msg, bool newLine)
        {
            if (newLine)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    messages.AppendText(msg + Environment.NewLine);
                }));
            }
            else
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    messages.AppendText(msg);
                }));
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            startBtn.IsEnabled = false;
            this.StartListener(PORT);
        }

        private void port_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                startBtn_Click(sender, e);
            }
        }
    }
}
